#!/bin/bash

terraform version
terraform init --input=false /code/gitRoot/terraform
terraform plan --input=false --out=terraform.plan -var aws-access-key=${aib.getParam("terraform.aws-access-key")} -var aws-secret-key=${aib.getParam("terraform.aws-secret-key")} /code/gitRoot/terraform
terraform apply --input=false "terraform.plan"