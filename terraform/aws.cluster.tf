# AWS Cluster
data "aws_eks_cluster" "lambdademo-cluster" {
  name = "lambdademo-cluster"
}

output "endpoint" {
  value = "${data.aws_eks_cluster.lambdademo-cluster.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${data.aws_eks_cluster.lambdademo-cluster.certificate_authority.0.data}"
}