# Variables
variable "aws-access-key" {}
variable "aws-secret-key" {}

variable "project" {}
variable "region" {}
variable "username" {
  default = "admin"
}
variable "password" {}