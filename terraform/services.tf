resource "kubernetes_service" "app-master" {
  metadata {
    name = "app-master"

    labels {
      app  = "lambdademodemo"
    }
  }

  spec {
    selector {
      app  = "lambdademo"
    }
    port {
      name        = "app-port"
      port        = 8080
      target_port = 8080
    }

    port {
      name        = "mysql-port"
      port        = 3606
      target_port = 3606
    }

    type = "LoadBalancer"
  }
  
}
