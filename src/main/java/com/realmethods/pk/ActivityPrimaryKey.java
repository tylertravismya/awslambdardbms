/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * Activity PrimaryKey class.
 *
 * @author    Dev Team
 */

// AIB : #getPrimaryKeyClassDecl() 
public class ActivityPrimaryKey extends BasePrimaryKey {
    //************************************************************************
    // Protected / Private Methods
    //************************************************************************

    //************************************************************************
    // Attributes
    //************************************************************************

    // DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
    // WITHIN THE Activity class.

    // AIB : #getKeyFieldDeclarations()
    public Long activityId;

    // ~AIB

    //************************************************************************
    // Public Methods
    //************************************************************************

    /**
     * default constructor - should be normally used for dynamic instantiation
     */
    public ActivityPrimaryKey() {
    }

    /**
     * single value constructor
     */
    public ActivityPrimaryKey(Object activityId) {
        this.activityId = (activityId != null)
            ? new Long(activityId.toString()) : null;
    }

    //************************************************************************
    // Access Methods
    //************************************************************************

    // AIB : #getKeyFieldAccessMethods()
    /**
         * Returns the activityId.
         * @return    Long
     */
    public Long getActivityId() {
        return (this.activityId);
    }

    /**
         * Assigns the activityId.
         * @return    Long
     */
    public void setActivityId(Long id) {
        this.activityId = id;
    }

    // ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys() {
        // assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();

        keys.add(activityId);

        return (keys);
    }

    public Object getFirstKey() {
        return (activityId);
    }

    // ~AIB 	        
}
