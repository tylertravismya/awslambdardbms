/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * Referrer PrimaryKey class.
 *
 * @author    Dev Team
 */

// AIB : #getPrimaryKeyClassDecl() 
public class ReferrerPrimaryKey extends BasePrimaryKey {
    //************************************************************************
    // Protected / Private Methods
    //************************************************************************

    //************************************************************************
    // Attributes
    //************************************************************************

    // DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
    // WITHIN THE Referrer class.

    // AIB : #getKeyFieldDeclarations()
    public Long referrerId;

    // ~AIB

    //************************************************************************
    // Public Methods
    //************************************************************************

    /**
     * default constructor - should be normally used for dynamic instantiation
     */
    public ReferrerPrimaryKey() {
    }

    /**
     * single value constructor
     */
    public ReferrerPrimaryKey(Object referrerId) {
        this.referrerId = (referrerId != null)
            ? new Long(referrerId.toString()) : null;
    }

    //************************************************************************
    // Access Methods
    //************************************************************************

    // AIB : #getKeyFieldAccessMethods()
    /**
         * Returns the referrerId.
         * @return    Long
     */
    public Long getReferrerId() {
        return (this.referrerId);
    }

    /**
         * Assigns the referrerId.
         * @return    Long
     */
    public void setReferrerId(Long id) {
        this.referrerId = id;
    }

    // ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys() {
        // assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();

        keys.add(referrerId);

        return (keys);
    }

    public Object getFirstKey() {
        return (referrerId);
    }

    // ~AIB 	        
}
