/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * QuestionGroup PrimaryKey class.
 *
 * @author    Dev Team
 */

// AIB : #getPrimaryKeyClassDecl() 
public class QuestionGroupPrimaryKey extends BasePrimaryKey {
    //************************************************************************
    // Protected / Private Methods
    //************************************************************************

    //************************************************************************
    // Attributes
    //************************************************************************

    // DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
    // WITHIN THE QuestionGroup class.

    // AIB : #getKeyFieldDeclarations()
    public Long questionGroupId;

    // ~AIB

    //************************************************************************
    // Public Methods
    //************************************************************************

    /**
     * default constructor - should be normally used for dynamic instantiation
     */
    public QuestionGroupPrimaryKey() {
    }

    /**
     * single value constructor
     */
    public QuestionGroupPrimaryKey(Object questionGroupId) {
        this.questionGroupId = (questionGroupId != null)
            ? new Long(questionGroupId.toString()) : null;
    }

    //************************************************************************
    // Access Methods
    //************************************************************************

    // AIB : #getKeyFieldAccessMethods()
    /**
         * Returns the questionGroupId.
         * @return    Long
     */
    public Long getQuestionGroupId() {
        return (this.questionGroupId);
    }

    /**
         * Assigns the questionGroupId.
         * @return    Long
     */
    public void setQuestionGroupId(Long id) {
        this.questionGroupId = id;
    }

    // ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys() {
        // assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();

        keys.add(questionGroupId);

        return (keys);
    }

    public Object getFirstKey() {
        return (questionGroupId);
    }

    // ~AIB 	        
}
