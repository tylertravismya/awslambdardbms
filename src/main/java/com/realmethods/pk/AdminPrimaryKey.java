/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * Admin PrimaryKey class.
 *
 * @author    Dev Team
 */

// AIB : #getPrimaryKeyClassDecl() 
public class AdminPrimaryKey extends BasePrimaryKey {
    //************************************************************************
    // Protected / Private Methods
    //************************************************************************

    //************************************************************************
    // Attributes
    //************************************************************************

    // DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
    // WITHIN THE Admin class.

    // AIB : #getKeyFieldDeclarations()
    public Long adminId;

    // ~AIB

    //************************************************************************
    // Public Methods
    //************************************************************************

    /**
     * default constructor - should be normally used for dynamic instantiation
     */
    public AdminPrimaryKey() {
    }

    /**
     * single value constructor
     */
    public AdminPrimaryKey(Object adminId) {
        this.adminId = (adminId != null) ? new Long(adminId.toString()) : null;
    }

    //************************************************************************
    // Access Methods
    //************************************************************************

    // AIB : #getKeyFieldAccessMethods()
    /**
         * Returns the adminId.
         * @return    Long
     */
    public Long getAdminId() {
        return (this.adminId);
    }

    /**
         * Assigns the adminId.
         * @return    Long
     */
    public void setAdminId(Long id) {
        this.adminId = id;
    }

    // ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys() {
        // assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();

        keys.add(adminId);

        return (keys);
    }

    public Object getFirstKey() {
        return (adminId);
    }

    // ~AIB 	        
}
