/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import com.realmethods.bo.*;

import com.realmethods.delegate.*;

import com.realmethods.primarykey.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;

import java.io.*;

import java.util.*;
import java.util.logging.*;


/**
 * Test QuestionGroup class.
 *
 * @author    Dev Team
 */
public class QuestionGroupTest {
    // attributes 
    protected QuestionGroupPrimaryKey thePrimaryKey = null;
    protected Properties frameworkProperties = null;
    private final Logger LOGGER = Logger.getLogger(QuestionGroup.class.getName());
    private Handler handler = null;
    private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";

    // constructors
    public QuestionGroupTest() {
        LOGGER.setUseParentHandlers(false); // only want to output to the provided LogHandler
    }

    // test methods
    @Test
    /**
     * Full Create-Read-Update-Delete of a QuestionGroup, through a QuestionGroupTest.
     */
    public void testCRUD() throws Throwable {
        try {
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("Beginning full test on QuestionGroupTest...");

            testCreate();
            testRead();
            testUpdate();
            testGetAll();
            testDelete();

            LOGGER.info("Successfully ran a full test on QuestionGroupTest...");
            LOGGER.info(
                "**********************************************************");
            LOGGER.info("");
        } catch (Throwable e) {
            throw e;
        } finally {
            if (handler != null) {
                handler.flush();
                LOGGER.removeHandler(handler);
            }
        }
    }

    /**
     * Tests creating a new QuestionGroup.
     *
     * @return    QuestionGroup
     */
    public QuestionGroup testCreate() throws Throwable {
        QuestionGroup businessObject = null;

        {
            LOGGER.info("QuestionGroupTest:testCreate()");
            LOGGER.info("-- Attempting to create a QuestionGroup");

            StringBuilder msg = new StringBuilder(
                    "-- Failed to create a QuestionGroup");

            try {
                businessObject = QuestionGroupBusinessDelegate.getQuestionGroupInstance()
                                                              .createQuestionGroup(getNewBO());
                assertNotNull(businessObject, msg.toString());

                thePrimaryKey = (QuestionGroupPrimaryKey) businessObject.getQuestionGroupPrimaryKey();
                assertNotNull(thePrimaryKey,
                    msg.toString() + " Contains a null primary key");

                LOGGER.info(
                    "-- Successfully created a QuestionGroup with primary key" +
                    thePrimaryKey);
            } catch (Exception e) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning(msg.toString() + businessObject);

                throw e;
            }
        }

        return businessObject;
    }

    /**
     * Tests reading a QuestionGroup.
     *
     * @return    QuestionGroup
     */
    public QuestionGroup testRead() throws Throwable {
        LOGGER.info("QuestionGroupTest:testRead()");
        LOGGER.info("-- Reading a previously created QuestionGroup");

        QuestionGroup businessObject = null;
        StringBuilder msg = new StringBuilder(
                "-- Failed to read QuestionGroup with primary key");
        msg.append(thePrimaryKey);

        try {
            businessObject = QuestionGroupBusinessDelegate.getQuestionGroupInstance()
                                                          .getQuestionGroup(thePrimaryKey);

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Successfully found QuestionGroup " +
                businessObject.toString());
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests updating a QuestionGroup.
     *
     * @return    QuestionGroup
     */
    public QuestionGroup testUpdate() throws Throwable {
        LOGGER.info("QuestionGroupTest:testUpdate()");
        LOGGER.info("-- Attempting to update a QuestionGroup.");

        StringBuilder msg = new StringBuilder(
                "Failed to update a QuestionGroup : ");
        QuestionGroup businessObject = null;

        try {
            businessObject = testCreate();

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Now updating the created QuestionGroup.");

            // for use later on...
            thePrimaryKey = (QuestionGroupPrimaryKey) businessObject.getQuestionGroupPrimaryKey();

            QuestionGroupBusinessDelegate proxy = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
            businessObject = proxy.saveQuestionGroup(businessObject);

            assertNotNull(businessObject, msg.toString());

            LOGGER.info("-- Successfully saved QuestionGroup - " +
                businessObject.toString());
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString() + " : primarykey-" + thePrimaryKey +
                " : businessObject-" + businessObject + " : " + e);

            throw e;
        }

        return (businessObject);
    }

    /**
     * Tests deleting a QuestionGroup.
     */
    public void testDelete() throws Throwable {
        LOGGER.info("QuestionGroupTest:testDelete()");
        LOGGER.info("-- Deleting a previously created QuestionGroup.");

        try {
            QuestionGroupBusinessDelegate.getQuestionGroupInstance()
                                         .delete(thePrimaryKey);

            LOGGER.info(
                "-- Successfully deleted QuestionGroup with primary key " +
                thePrimaryKey);
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(
                "-- Failed to delete QuestionGroup with primary key " +
                thePrimaryKey);

            throw e;
        }
    }

    /**
     * Tests getting all QuestionGroups.
     *
     * @return    Collection
     */
    public ArrayList<QuestionGroup> testGetAll() throws Throwable {
        LOGGER.info(
            "QuestionGroupTest:testGetAll() - Retrieving Collection of QuestionGroups:");

        StringBuilder msg = new StringBuilder(
                "-- Failed to get all QuestionGroup : ");
        ArrayList<QuestionGroup> collection = null;

        try {
            // call the static get method on the QuestionGroupBusinessDelegate
            collection = QuestionGroupBusinessDelegate.getQuestionGroupInstance()
                                                      .getAllQuestionGroup();

            if ((collection == null) || (collection.size() == 0)) {
                LOGGER.warning(unexpectedErrorMsg);
                LOGGER.warning("-- " + msg.toString() +
                    " Empty collection returned.");
            } else {
                // Now print out the values
                QuestionGroup currentBO = null;
                Iterator<QuestionGroup> iter = collection.iterator();

                while (iter.hasNext()) {
                    // Retrieve the businessObject   
                    currentBO = iter.next();

                    assertNotNull(currentBO,
                        "-- null value object in Collection.");
                    assertNotNull(currentBO.getQuestionGroupPrimaryKey(),
                        "-- value object in Collection has a null primary key");

                    LOGGER.info(" - " + currentBO.toString());
                }
            }
        } catch (Throwable e) {
            LOGGER.warning(unexpectedErrorMsg);
            LOGGER.warning(msg.toString());

            throw e;
        }

        return (collection);
    }

    public QuestionGroupTest setHandler(Handler handler) {
        this.handler = handler;
        LOGGER.addHandler(handler); // assign so the LOGGER can only output results to the Handler

        return this;
    }

    /**
     * Returns a new populate QuestionGroup
     *
     * @return    QuestionGroup
     */
    protected QuestionGroup getNewBO() {
        QuestionGroup newBO = new QuestionGroup();

        // AIB : \#defaultBOOutput() 
        newBO.setAggregateScore(new java.lang.Double(
                new String(
                    org.apache.commons.lang3.RandomStringUtils.randomNumeric(3))));
        newBO.setWeight(new java.lang.Double(
                new String(
                    org.apache.commons.lang3.RandomStringUtils.randomNumeric(3))));
        newBO.setName(new String(
                org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10)));

        // ~AIB
        return (newBO);
    }
}
