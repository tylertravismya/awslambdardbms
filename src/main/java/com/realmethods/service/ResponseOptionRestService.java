/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.realmethods.bo.*;
import com.realmethods.bo.Base;

import com.realmethods.common.JsonTransformer;

import com.realmethods.delegate.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.io.StringWriter;

import java.text.SimpleDateFormat;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * Implements Struts action processing for business entity ResponseOption.
 *
 * @author Dev Team
 */
public class ResponseOptionRestService extends BaseRestService {
    private static final Logger LOGGER = Logger.getLogger(BaseRestService.class.getName());

    //************************************************************************    
    // Attributes
    //************************************************************************
    private ResponseOption responseOption = null;

    public ResponseOptionRestService() {
    }

    /**
     * Handles saving a ResponseOption BO.  if not key provided, calls create, otherwise calls save
     * @exception        ProcessingException
     */
    protected ResponseOption save() throws ProcessingException {
        // doing it here helps
        getResponseOption();

        LOGGER.info("ResponseOption.save() on - " + responseOption);

        if (hasPrimaryKey()) {
            return (update());
        } else {
            return (create());
        }
    }

    /**
     * Returns true if the responseOption is non-null and has it's primary key field(s) set
     * @return                boolean
     */
    protected boolean hasPrimaryKey() {
        boolean hasPK = false;

        if ((responseOption != null) &&
                (responseOption.getResponseOptionPrimaryKey().hasBeenAssigned() == true)) {
            hasPK = true;
        }

        return (hasPK);
    }

    /**
     * Handles updating a ResponseOption BO
     * @return                ResponseOption
     * @exception        ProcessingException
     */
    protected ResponseOption update() throws ProcessingException {
        // store provided data
        ResponseOption tmp = responseOption;

        // load actual data from storage
        loadHelper(responseOption.getResponseOptionPrimaryKey());

        // copy provided data into actual data
        responseOption.copyShallow(tmp);

        try {
            // create the ResponseOptionBusiness Delegate            
            ResponseOptionBusinessDelegate delegate = ResponseOptionBusinessDelegate.getResponseOptionInstance();
            this.responseOption = delegate.saveResponseOption(responseOption);

            if (this.responseOption != null) {
                LOGGER.info(
                    "ResponseOptionRestService:update() - successfully updated ResponseOption - " +
                    responseOption.toString());
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:update() - successfully update ResponseOption - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.responseOption;
    }

    /**
     * Handles creating a ResponseOption BO
     * @return                ResponseOption
     */
    protected ResponseOption create() throws ProcessingException {
        try {
            responseOption = getResponseOption();
            this.responseOption = ResponseOptionBusinessDelegate.getResponseOptionInstance()
                                                                .createResponseOption(responseOption);
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:create() - exception ResponseOption - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.responseOption;
    }

    /**
     * Handles deleting a ResponseOption BO
     * @exception        ProcessingException
     */
    protected void delete() throws ProcessingException {
        try {
            ResponseOptionBusinessDelegate delegate = ResponseOptionBusinessDelegate.getResponseOptionInstance();

            Long[] childIds = getChildIds();

            if ((childIds == null) || (childIds.length == 0)) {
                Long responseOptionId = parseId("responseOptionId");
                delegate.delete(new ResponseOptionPrimaryKey(responseOptionId));
                LOGGER.info(
                    "ResponseOptionRestService:delete() - successfully deleted ResponseOption with key " +
                    responseOption.getResponseOptionPrimaryKey()
                                  .valuesAsCollection());
            } else {
                for (Long id : childIds) {
                    try {
                        delegate.delete(new ResponseOptionPrimaryKey(id));
                    } catch (Throwable exc) {
                        signalBadRequest();

                        String errMsg = "ResponseOptionRestService:delete() - " +
                            exc.getMessage();
                        LOGGER.severe(errMsg);
                        throw new ProcessingException(errMsg);
                    }
                }
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:delete() - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }
    }

    /**
     * Handles loading a ResponseOption BO
     * @param                Long responseOptionId
     * @exception        ProcessingException
     * @return                ResponseOption
     */
    protected ResponseOption load() throws ProcessingException {
        ResponseOptionPrimaryKey pk = null;
        Long responseOptionId = parseId("responseOptionId");

        try {
            LOGGER.info("ResponseOption.load pk is " + responseOptionId);

            if (responseOptionId != null) {
                pk = new ResponseOptionPrimaryKey(responseOptionId);

                loadHelper(pk);

                // load the contained instance of ResponseOption
                this.responseOption = ResponseOptionBusinessDelegate.getResponseOptionInstance()
                                                                    .getResponseOption(pk);

                LOGGER.info(
                    "ResponseOptionRestService:load() - successfully loaded - " +
                    this.responseOption.toString());
            } else {
                signalBadRequest();

                String errMsg = "ResponseOptionRestService:load() - unable to locate the primary key as an attribute or a selection for - " +
                    responseOption.toString();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:load() - failed to load ResponseOption using Id " +
                responseOptionId + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return responseOption;
    }

    /**
     * Handles loading all ResponseOption business objects
     * @return                List<ResponseOption>
     * @exception        ProcessingException
     */
    protected List<ResponseOption> loadAll() throws ProcessingException {
        List<ResponseOption> responseOptionList = null;

        try {
            // load the ResponseOption
            responseOptionList = ResponseOptionBusinessDelegate.getResponseOptionInstance()
                                                               .getAllResponseOption();

            if (responseOptionList != null) {
                LOGGER.info(
                    "ResponseOptionRestService:loadAllResponseOption() - successfully loaded all ResponseOptions");
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:loadAll() - failed to load all ResponseOptions - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return responseOptionList;
    }

    protected ResponseOption loadHelper(ResponseOptionPrimaryKey pk)
        throws ProcessingException {
        try {
            LOGGER.info("ResponseOption.loadHelper primary key is " + pk);

            if (pk != null) {
                // load the contained instance of ResponseOption
                this.responseOption = ResponseOptionBusinessDelegate.getResponseOptionInstance()
                                                                    .getResponseOption(pk);

                LOGGER.info(
                    "ResponseOptionRestService:loadHelper() - successfully loaded - " +
                    this.responseOption.toString());
            } else {
                signalBadRequest();

                String errMsg = "ResponseOptionRestService:loadHelper() - null primary key provided.";
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ResponseOptionRestService:load() - failed to load ResponseOption using pk " +
                pk + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return responseOption;
    }

    // overloads from BaseRestService

    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
    public Object handleExec(String action, spark.Response response,
        spark.Request request) throws ProcessingException {
        // store locally
        this.response = response;
        this.request = request;

        if (action == null) {
            signalBadRequest();
            throw new ProcessingException();
        }

        Object returnVal = null;

        switch (action) {
        case "save":
            returnVal = save();

            break;

        case "load":
            returnVal = load();

            break;

        case "delete":
            delete();

            break;

        case "loadAll":
        case "viewAll":
            returnVal = loadAll();

            break;

        default:
            signalBadRequest();
            throw new ProcessingException(
                "ResponseOption.execute(...) - unable to handle action " +
                action);
        }

        return returnVal;
    }

    /**
     * Uses ObjectMapper to map from Json to a ResponseOption. Found in the request body.
     *
     * @return ResponseOption
     */
    private ResponseOption getResponseOption() {
        if (responseOption == null) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
                responseOption = mapper.readValue(java.net.URLDecoder.decode(
                            request.queryString(), "UTF-8"),
                        ResponseOption.class);
            } catch (Exception exc) {
                signalBadRequest();
                LOGGER.severe(
                    "ResponseOptionRestService.getResponseOption() - failed to Json map from String to ResponseOption - " +
                    exc.getMessage());
            }
        }

        return (responseOption);
    }

    protected String getSubclassName() {
        return ("ResponseOptionRestService");
    }
}
