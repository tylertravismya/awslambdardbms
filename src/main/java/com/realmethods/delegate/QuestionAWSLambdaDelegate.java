/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.delegate;

import com.amazonaws.services.lambda.runtime.Context;

import com.realmethods.bo.*;

import com.realmethods.exception.CreationException;
import com.realmethods.exception.DeletionException;
import com.realmethods.exception.NotFoundException;
import com.realmethods.exception.SaveException;

import com.realmethods.primarykey.*;

import io.swagger.annotations.*;

import java.io.IOException;

import java.util.*;

import javax.ws.rs.*;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;


/**
 * Question AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Question related services in the case of a Question business related service failing.</li>
 * <li>Exposes a simpler, uniform Question interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Question business related services.</li>
 * </ol>
 * <p>
 * @author Dev Team
 */
@Api(value = "Question", description = "RESTful API to interact with Question resources.")
@Path("/Question")
public class QuestionAWSLambdaDelegate extends BaseAWSLambdaDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    //    private static final Logger LOGGER = Logger.getLogger(QuestionAWSLambdaDelegate.class.getName());
    private static final String PACKAGE_NAME = "Question";

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public QuestionAWSLambdaDelegate() {
    }

    /**
     * Creates the provided Question
     * @param                businessObject         Question
         * @param                context                Context
     * @return             Question
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Question", notes = "Creates Question using the provided data")
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Question createQuestion(
        @ApiParam(value = "Question entity to create", required = true)
    Question businessObject, Context context) throws CreationException {
        if (businessObject == null) {
            String errMsg = "Null Question provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        }

        try {
            String actionName = "save";
            String result = call(PACKAGE_NAME, actionName, businessObject);
            businessObject = (Question) fromJson(result, Question.class);
        } catch (Exception exc) {
            String errMsg = "QuestionAWSLambdaDelegate:createQuestion() - Unable to create Question" +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        } finally {
        }

        return (businessObject);
    }

    /**
     * Method to retrieve the Question via a supplied QuestionPrimaryKey.
     * @param         key
         * @param        context                Context
     * @return         Question
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Question", notes = "Gets the Question associated with the provided primary key", response = Question.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public static Question getQuestion(
        @ApiParam(value = "Question primary key", required = true)
    QuestionPrimaryKey key, Context context) throws NotFoundException {
        Question businessObject = null;

        try {
            String actionName = "load";
            String result = call(PACKAGE_NAME, actionName, key);
            businessObject = (Question) fromJson(result, Question.class);
        } catch (Exception exc) {
            String errMsg = "Unable to locate Question with key " +
                key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return businessObject;
    }

    /**
     * Saves the provided Question
     * @param                businessObject                Question
         * @param                context                Context
     * @return       what was just saved
     * @exception    SaveException
     */
    @ApiOperation(value = "Saves a Question", notes = "Saves Question using the provided data")
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Question saveQuestion(
        @ApiParam(value = "Question entity to save", required = true)
    Question businessObject, Context context) throws SaveException {
        if (businessObject == null) {
            String errMsg = "Null Question provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        QuestionPrimaryKey key = businessObject.getQuestionPrimaryKey();

        if (key != null) {
            try {
                String actionName = "save";
                String result = call(PACKAGE_NAME, actionName, businessObject);
                businessObject = (Question) fromJson(result, Question.class);
            } catch (Exception exc) {
                String errMsg = "Unable to save Question" +
                    getContextDetails(context) + exc;
                context.getLogger().log(errMsg);
                throw new SaveException(errMsg);
            } finally {
            }
        } else {
            String errMsg = "Unable to create Question due to it having a null QuestionPrimaryKey.";
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        return (businessObject);
    }

    /**
    * Method to retrieve a collection of all Questions
    * @param                context                Context
    * @return         ArrayList<Question>
    */
    @ApiOperation(value = "Get all Question", notes = "Get all Question from storage", responseContainer = "ArrayList", response = Question.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public static ArrayList<Question> getAllQuestion(Context context)
        throws NotFoundException {
        ArrayList<Question> array = null;

        try {
            String actionName = "viewAll";
            String result = call(PACKAGE_NAME, actionName, null);
            array = (ArrayList<Question>) fromJson(result, ArrayList.class);
        } catch (Exception exc) {
            String errMsg = "failed to getAllQuestion - " +
                getContextDetails(context) + exc.getMessage();
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return array;
    }

    /**
     * Deletes the associated business object using the provided primary key.
     * @param                key         QuestionPrimaryKey
     * @param                context                Context
     * @exception         DeletionException
     */
    @ApiOperation(value = "Deletes a Question", notes = "Deletes the Question associated with the provided primary key", response = Question.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public static void deleteQuestion(
        @ApiParam(value = "Question primary key", required = true)
    QuestionPrimaryKey key, Context context) throws DeletionException {
        if (key == null) {
            String errMsg = "Null key provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        }

        try {
            String actionName = "delete";
            String result = call(PACKAGE_NAME, actionName, key);
        } catch (Exception exc) {
            String errMsg = "Unable to delete Question using key = " + key +
                ". " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        } finally {
        }

        return;
    }

    // role related methods

    /**
     * Retrieves the Responses on a Question
     * @param                parentKey        QuestionPrimaryKey
     * @param                context                Context
     * @return            Set<ResponseOption>
     * @exception        NotFoundException
     */
    public static Set<ResponseOption> getResponses(
        QuestionPrimaryKey parentKey, Context context)
        throws NotFoundException {
        Question question = getQuestion(parentKey, context);

        return (question.getResponses());
    }

    /**
     * Add the assigned ResponseOption into the Responses of the relevant Question
     * @param                parentKey        QuestionPrimaryKey
     * @param                childKey        ResponseOptionPrimaryKey
         * @param                context                Context
     * @return            Question
     * @exception        NotFoundException
     */
    public static Question addResponses(QuestionPrimaryKey parentKey,
        ResponseOptionPrimaryKey childKey, Context context)
        throws SaveException, NotFoundException {
        Question question = getQuestion(parentKey, context);

        // find the ResponseOption
        ResponseOption child = ResponseOptionAWSLambdaDelegate.getResponseOption(childKey,
                context);

        // add it to the Responses 
        question.getResponses().add(child);

        // save the Question
        question = QuestionAWSLambdaDelegate.saveQuestion(question, context);

        return (question);
    }

    /**
     * Saves multiple ResponseOption entities as the Responses to the relevant Question
     * @param                parentKey        QuestionPrimaryKey
     * @param                List<ResponseOptionPrimaryKey> childKeys
     * @return            Question
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public Question assignResponses(QuestionPrimaryKey parentKey,
        List<ResponseOptionPrimaryKey> childKeys, Context context)
        throws SaveException, NotFoundException {
        Question question = getQuestion(parentKey, context);

        // clear out the Responses 
        question.getResponses().clear();

        // finally, find each child and add
        if (childKeys != null) {
            ResponseOption child = null;

            for (ResponseOptionPrimaryKey childKey : childKeys) {
                // retrieve the ResponseOption
                child = ResponseOptionAWSLambdaDelegate.getResponseOption(childKey,
                        context);

                // add it to the Responses List
                question.getResponses().add(child);
            }
        }

        // save the Question
        question = QuestionAWSLambdaDelegate.saveQuestion(question, context);

        return (question);
    }

    /**
     * Delete multiple ResponseOption entities as the Responses to the relevant Question
     * @param                parentKey        QuestionPrimaryKey
     * @param                List<ResponseOptionPrimaryKey> childKeys
     * @return            Question
     * @exception        DeletionException
     * @exception        NotFoundException
     * @exception        SaveException
     */
    public Question deleteResponses(QuestionPrimaryKey parentKey,
        List<ResponseOptionPrimaryKey> childKeys, Context context)
        throws DeletionException, NotFoundException, SaveException {
        Question question = getQuestion(parentKey, context);

        if (childKeys != null) {
            Set<ResponseOption> children = question.getResponses();
            ResponseOption child = null;

            for (ResponseOptionPrimaryKey childKey : childKeys) {
                try {
                    // first remove the relevant child from the list
                    child = ResponseOptionAWSLambdaDelegate.getResponseOption(childKey,
                            context);
                    children.remove(child);

                    // then safe to delete the child				
                    ResponseOptionAWSLambdaDelegate.deleteResponseOption(childKey,
                        context);
                } catch (Exception exc) {
                    String errMsg = "Deletion failed - " + exc.getMessage();
                    context.getLogger().log(errMsg);
                    throw new DeletionException(errMsg);
                }
            }

            // assign the modified list of ResponseOption back to the question
            question.setResponses(children);
            // save it 
            question = QuestionAWSLambdaDelegate.saveQuestion(question, context);
        }

        return (question);
    }
}
