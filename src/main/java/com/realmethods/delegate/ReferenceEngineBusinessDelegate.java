/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.delegate;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.*;

import com.realmethods.primarykey.*;

import java.io.IOException;

import java.util.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * ReferenceEngine business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceEngine related services in the case of a ReferenceEngine business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceEngine interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceEngine business related services.</li>
 * </ol>
 * <p>
 * @author Dev Team
 */
public class ReferenceEngineBusinessDelegate extends BaseBusinessDelegate {
    // AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
    // ~AIB

    //************************************************************************
    // Attributes
    //************************************************************************

    /**
     * Singleton instance
     */
    protected static ReferenceEngineBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(ReferenceEngine.class.getName());

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public ReferenceEngineBusinessDelegate() {
    }

    /**
         * ReferenceEngine Business Delegate Factory Method
         *
         * Returns a singleton instance of ReferenceEngineBusinessDelegate().
         * All methods are expected to be self-sufficient.
         *
         * @return         ReferenceEngineBusinessDelegate
         */
    public static ReferenceEngineBusinessDelegate getReferenceEngineInstance() {
        if (singleton == null) {
            singleton = new ReferenceEngineBusinessDelegate();
        }

        return (singleton);
    }

    /**
     * Method to retrieve the ReferenceEngine via an ReferenceEnginePrimaryKey.
     * @param         key
     * @return         ReferenceEngine
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException
     */
    public ReferenceEngine getReferenceEngine(ReferenceEnginePrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ReferenceEngineBusinessDelegate:getReferenceEngine - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        ReferenceEngine returnBO = null;

        ReferenceEngineDAO dao = getReferenceEngineDAO();

        try {
            returnBO = dao.findReferenceEngine(key);
        } catch (Exception exc) {
            String errMsg = "ReferenceEngineBusinessDelegate:getReferenceEngine( ReferenceEnginePrimaryKey key ) - unable to locate ReferenceEngine with key " +
                key.toString() + " - " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseReferenceEngineDAO(dao);
        }

        return returnBO;
    }

    /**
     * Method to retrieve a collection of all ReferenceEngines
     *
     * @return         ArrayList<ReferenceEngine>
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<ReferenceEngine> getAllReferenceEngine()
        throws ProcessingException {
        String msgPrefix = "ReferenceEngineBusinessDelegate:getAllReferenceEngine() - ";
        ArrayList<ReferenceEngine> array = null;

        ReferenceEngineDAO dao = getReferenceEngineDAO();

        try {
            array = dao.findAllReferenceEngine();
        } catch (Exception exc) {
            String errMsg = msgPrefix + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseReferenceEngineDAO(dao);
        }

        return array;
    }

    /**
     * Creates the provided BO.
     * @param                businessObject         ReferenceEngine
     * @return       ReferenceEngine
     * @exception    ProcessingException
     * @exception        IllegalArgumentException
     */
    public ReferenceEngine createReferenceEngine(ReferenceEngine businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ReferenceEngineBusinessDelegate:createReferenceEngine - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // return value once persisted
        ReferenceEngineDAO dao = getReferenceEngineDAO();

        try {
            businessObject = dao.createReferenceEngine(businessObject);
        } catch (Exception exc) {
            String errMsg = "ReferenceEngineBusinessDelegate:createReferenceEngine() - Unable to create ReferenceEngine" +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseReferenceEngineDAO(dao);
        }

        return (businessObject);
    }

    /**
     * Saves the underlying BO.
     * @param                businessObject                ReferenceEngine
     * @return       what was just saved
     * @exception    ProcessingException
     * @exception          IllegalArgumentException
     */
    public ReferenceEngine saveReferenceEngine(ReferenceEngine businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ReferenceEngineBusinessDelegate:saveReferenceEngine - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceEnginePrimaryKey key = businessObject.getReferenceEnginePrimaryKey();

        if (key != null) {
            ReferenceEngineDAO dao = getReferenceEngineDAO();

            try {
                businessObject = (ReferenceEngine) dao.saveReferenceEngine(businessObject);
            } catch (Exception exc) {
                String errMsg = "ReferenceEngineBusinessDelegate:saveReferenceEngine() - Unable to save ReferenceEngine" +
                    exc;
                LOGGER.warning(errMsg);
                throw new ProcessingException(errMsg);
            } finally {
                releaseReferenceEngineDAO(dao);
            }
        } else {
            String errMsg = "ReferenceEngineBusinessDelegate:saveReferenceEngine() - Unable to create ReferenceEngine due to it having a null ReferenceEnginePrimaryKey.";

            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        }

        return (businessObject);
    }

    /**
     * Deletes the associatied value object using the provided primary key.
     * @param                key         ReferenceEnginePrimaryKey
     * @exception         ProcessingException
     */
    public void delete(ReferenceEnginePrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ReferenceEngineBusinessDelegate:saveReferenceEngine - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        ReferenceEngineDAO dao = getReferenceEngineDAO();

        try {
            dao.deleteReferenceEngine(key);
        } catch (Exception exc) {
            String errMsg = msgPrefix +
                "Unable to delete ReferenceEngine using key = " + key + ". " +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseReferenceEngineDAO(dao);
        }

        return;
    }

    // business methods
    /**
     * Returns the ReferenceEngine specific DAO.
     *
     * @return      ReferenceEngine DAO
     */
    public ReferenceEngineDAO getReferenceEngineDAO() {
        return (new com.realmethods.dao.ReferenceEngineDAO());
    }

    /**
     * Release the ReferenceEngineDAO back to the FrameworkDAOFactory
     */
    public void releaseReferenceEngineDAO(
        com.realmethods.dao.ReferenceEngineDAO dao) {
        dao = null;
    }
}
