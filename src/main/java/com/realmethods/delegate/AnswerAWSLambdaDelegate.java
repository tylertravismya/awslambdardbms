/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.delegate;

import com.amazonaws.services.lambda.runtime.Context;

import com.realmethods.bo.*;

import com.realmethods.exception.CreationException;
import com.realmethods.exception.DeletionException;
import com.realmethods.exception.NotFoundException;
import com.realmethods.exception.SaveException;

import com.realmethods.primarykey.*;

import io.swagger.annotations.*;

import java.io.IOException;

import java.util.*;

import javax.ws.rs.*;

//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;


/**
 * Answer AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Answer related services in the case of a Answer business related service failing.</li>
 * <li>Exposes a simpler, uniform Answer interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Answer business related services.</li>
 * </ol>
 * <p>
 * @author Dev Team
 */
@Api(value = "Answer", description = "RESTful API to interact with Answer resources.")
@Path("/Answer")
public class AnswerAWSLambdaDelegate extends BaseAWSLambdaDelegate {
    //************************************************************************
    // Attributes
    //************************************************************************

    //    private static final Logger LOGGER = Logger.getLogger(AnswerAWSLambdaDelegate.class.getName());
    private static final String PACKAGE_NAME = "Answer";

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public AnswerAWSLambdaDelegate() {
    }

    /**
     * Creates the provided Answer
     * @param                businessObject         Answer
         * @param                context                Context
     * @return             Answer
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Answer", notes = "Creates Answer using the provided data")
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Answer createAnswer(
        @ApiParam(value = "Answer entity to create", required = true)
    Answer businessObject, Context context) throws CreationException {
        if (businessObject == null) {
            String errMsg = "Null Answer provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        }

        try {
            String actionName = "save";
            String result = call(PACKAGE_NAME, actionName, businessObject);
            businessObject = (Answer) fromJson(result, Answer.class);
        } catch (Exception exc) {
            String errMsg = "AnswerAWSLambdaDelegate:createAnswer() - Unable to create Answer" +
                getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new CreationException(errMsg);
        } finally {
        }

        return (businessObject);
    }

    /**
     * Method to retrieve the Answer via a supplied AnswerPrimaryKey.
     * @param         key
         * @param        context                Context
     * @return         Answer
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Answer", notes = "Gets the Answer associated with the provided primary key", response = Answer.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public static Answer getAnswer(
        @ApiParam(value = "Answer primary key", required = true)
    AnswerPrimaryKey key, Context context) throws NotFoundException {
        Answer businessObject = null;

        try {
            String actionName = "load";
            String result = call(PACKAGE_NAME, actionName, key);
            businessObject = (Answer) fromJson(result, Answer.class);
        } catch (Exception exc) {
            String errMsg = "Unable to locate Answer with key " +
                key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return businessObject;
    }

    /**
     * Saves the provided Answer
     * @param                businessObject                Answer
         * @param                context                Context
     * @return       what was just saved
     * @exception    SaveException
     */
    @ApiOperation(value = "Saves a Answer", notes = "Saves Answer using the provided data")
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Answer saveAnswer(
        @ApiParam(value = "Answer entity to save", required = true)
    Answer businessObject, Context context) throws SaveException {
        if (businessObject == null) {
            String errMsg = "Null Answer provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AnswerPrimaryKey key = businessObject.getAnswerPrimaryKey();

        if (key != null) {
            try {
                String actionName = "save";
                String result = call(PACKAGE_NAME, actionName, businessObject);
                businessObject = (Answer) fromJson(result, Answer.class);
            } catch (Exception exc) {
                String errMsg = "Unable to save Answer" +
                    getContextDetails(context) + exc;
                context.getLogger().log(errMsg);
                throw new SaveException(errMsg);
            } finally {
            }
        } else {
            String errMsg = "Unable to create Answer due to it having a null AnswerPrimaryKey.";
            context.getLogger().log(errMsg);
            throw new SaveException(errMsg);
        }

        return (businessObject);
    }

    /**
    * Method to retrieve a collection of all Answers
    * @param                context                Context
    * @return         ArrayList<Answer>
    */
    @ApiOperation(value = "Get all Answer", notes = "Get all Answer from storage", responseContainer = "ArrayList", response = Answer.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public static ArrayList<Answer> getAllAnswer(Context context)
        throws NotFoundException {
        ArrayList<Answer> array = null;

        try {
            String actionName = "viewAll";
            String result = call(PACKAGE_NAME, actionName, null);
            array = (ArrayList<Answer>) fromJson(result, ArrayList.class);
        } catch (Exception exc) {
            String errMsg = "failed to getAllAnswer - " +
                getContextDetails(context) + exc.getMessage();
            context.getLogger().log(errMsg);
            throw new NotFoundException(errMsg);
        } finally {
        }

        return array;
    }

    /**
     * Deletes the associated business object using the provided primary key.
     * @param                key         AnswerPrimaryKey
     * @param                context                Context
     * @exception         DeletionException
     */
    @ApiOperation(value = "Deletes a Answer", notes = "Deletes the Answer associated with the provided primary key", response = Answer.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public static void deleteAnswer(
        @ApiParam(value = "Answer primary key", required = true)
    AnswerPrimaryKey key, Context context) throws DeletionException {
        if (key == null) {
            String errMsg = "Null key provided but not allowed " +
                getContextDetails(context);
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        }

        try {
            String actionName = "delete";
            String result = call(PACKAGE_NAME, actionName, key);
        } catch (Exception exc) {
            String errMsg = "Unable to delete Answer using key = " + key +
                ". " + getContextDetails(context) + exc;
            context.getLogger().log(errMsg);
            throw new DeletionException(errMsg);
        } finally {
        }

        return;
    }

    // role related methods

    /**
     * Gets the Question using the provided primary key of a Answer
     * @param                parentKey        AnswerPrimaryKey
     * @return            Question
     * @exception        NotFoundException
     */
    public static Question getQuestion(AnswerPrimaryKey parentKey,
        Context context) throws NotFoundException {
        Answer answer = getAnswer(parentKey, context);
        QuestionPrimaryKey childKey = answer.getQuestion()
                                            .getQuestionPrimaryKey();
        Question child = QuestionAWSLambdaDelegate.getQuestion(childKey, context);

        return (child);
    }

    /**
     * Assigns the Question on a Answer using the provided primary key of a Question
     * @param                parentKey        AnswerPrimaryKey
     * @param                parentKey        AnswerPrimaryKey
     * @param                context                Context
     * @return            Answer
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static Answer saveQuestion(AnswerPrimaryKey parentKey,
        QuestionPrimaryKey childKey, Context context)
        throws SaveException, NotFoundException {
        Answer answer = getAnswer(parentKey, context);
        Question child = QuestionAWSLambdaDelegate.getQuestion(childKey, context);

        // assign the Question
        answer.setQuestion(child);

        // save the Answer 
        answer = AnswerAWSLambdaDelegate.saveAnswer(answer, context);

        return (answer);
    }

    /**
     * Unassigns the Question on a Answer
     * @param                parentKey        AnswerPrimaryKey
     * @param                Context                context
     * @return            Answer
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static Answer deleteQuestion(AnswerPrimaryKey parentKey,
        Context context)
        throws DeletionException, NotFoundException, SaveException {
        Answer answer = getAnswer(parentKey, context);

        if (answer.getQuestion() != null) {
            QuestionPrimaryKey pk = answer.getQuestion().getQuestionPrimaryKey();

            // first null out the Question on the parent so there's no constraint during deletion
            answer.setQuestion(null);
            AnswerAWSLambdaDelegate.saveAnswer(answer, context);

            // now it is safe to delete the Question 
            QuestionAWSLambdaDelegate.deleteQuestion(pk, context);
        }

        return (answer);
    }

    /**
     * Gets the Response using the provided primary key of a Answer
     * @param                parentKey        AnswerPrimaryKey
     * @return            ResponseOption
     * @exception        NotFoundException
     */
    public static ResponseOption getResponse(AnswerPrimaryKey parentKey,
        Context context) throws NotFoundException {
        Answer answer = getAnswer(parentKey, context);
        ResponseOptionPrimaryKey childKey = answer.getResponse()
                                                  .getResponseOptionPrimaryKey();
        ResponseOption child = ResponseOptionAWSLambdaDelegate.getResponseOption(childKey,
                context);

        return (child);
    }

    /**
     * Assigns the Response on a Answer using the provided primary key of a ResponseOption
     * @param                parentKey        AnswerPrimaryKey
     * @param                parentKey        AnswerPrimaryKey
     * @param                context                Context
     * @return            Answer
     * @exception        SaveException
     * @exception        NotFoundException
     */
    public static Answer saveResponse(AnswerPrimaryKey parentKey,
        ResponseOptionPrimaryKey childKey, Context context)
        throws SaveException, NotFoundException {
        Answer answer = getAnswer(parentKey, context);
        ResponseOption child = ResponseOptionAWSLambdaDelegate.getResponseOption(childKey,
                context);

        // assign the Response
        answer.setResponse(child);

        // save the Answer 
        answer = AnswerAWSLambdaDelegate.saveAnswer(answer, context);

        return (answer);
    }

    /**
     * Unassigns the Response on a Answer
     * @param                parentKey        AnswerPrimaryKey
     * @param                Context                context
     * @return            Answer
     * @exception        SaveException
     * @exception        NotFoundException
         * @exception        SaveException
     */
    public static Answer deleteResponse(AnswerPrimaryKey parentKey,
        Context context)
        throws DeletionException, NotFoundException, SaveException {
        Answer answer = getAnswer(parentKey, context);

        if (answer.getResponse() != null) {
            ResponseOptionPrimaryKey pk = answer.getResponse()
                                                .getResponseOptionPrimaryKey();

            // first null out the ResponseOption on the parent so there's no constraint during deletion
            answer.setResponse(null);
            AnswerAWSLambdaDelegate.saveAnswer(answer, context);

            // now it is safe to delete the Response 
            ResponseOptionAWSLambdaDelegate.deleteResponseOption(pk, context);
        }

        return (answer);
    }
}
