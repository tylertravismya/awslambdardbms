/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity ResponseOption.
 *
 * @author Dev Team
 */

// AIB : #getDAOClassDecl()
public class ResponseOptionDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(ResponseOption.class.getName());

    /**
     * default constructor
     */
    public ResponseOptionDAO() {
    }

    /**
     * Retrieves a ResponseOption from the persistent store, using the provided primary key.
     * If no match is found, a null ResponseOption is returned.
     * <p>
     * @param       pk
     * @return      ResponseOption
     * @exception   ProcessingException
     */
    public ResponseOption findResponseOption(ResponseOptionPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ResponseOptionDAO.findResponseOption(...) cannot have a null primary key argument");
        }

        Query query = null;
        ResponseOption businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.realmethods.bo.ResponseOption as responseoption where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("responseoption.responseOptionId = " +
                pk.getResponseOptionId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new ResponseOption();
                businessObject.copy((ResponseOption) query.list().iterator()
                                                          .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ResponseOptionDAO.findResponseOption failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ResponseOptionDAO.findResponseOption - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all ResponseOptions
     * @return                ArrayList<ResponseOption>
     * @exception   ProcessingException
     */
    public ArrayList<ResponseOption> findAllResponseOption()
        throws ProcessingException {
        ArrayList<ResponseOption> list = new ArrayList<ResponseOption>();
        ArrayList<ResponseOption> refList = new ArrayList<ResponseOption>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.realmethods.bo.ResponseOption");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<ResponseOption>) query.list();

                ResponseOption tmp = null;

                for (ResponseOption listEntry : list) {
                    tmp = new ResponseOption();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ResponseOptionDAO.findAllResponseOption failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ResponseOptionDAO.findAllResponseOption - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "ResponseOptionDAO:findAllResponseOptions() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new ResponseOption into the persistent store.
     * @param       businessObject
     * @return      newly persisted ResponseOption
     * @exception   ProcessingException
     */
    public ResponseOption createResponseOption(ResponseOption businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ResponseOptionDAO.createResponseOption - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ResponseOptionDAO.createResponseOption failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ResponseOptionDAO.createResponseOption - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided ResponseOption to the persistent store.
     *
     * @param       businessObject
     * @return      ResponseOption        stored entity
     * @exception   ProcessingException
     */
    public ResponseOption saveResponseOption(ResponseOption businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ResponseOptionDAO.saveResponseOption - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ResponseOptionDAO.saveResponseOption failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ResponseOptionDAO.saveResponseOption - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a ResponseOption from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteResponseOption(ResponseOptionPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            ResponseOption bo = findResponseOption(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ResponseOptionDAO.deleteResponseOption - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ResponseOptionDAO.deleteResponseOption failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ResponseOptionDAO.deleteResponseOption - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
