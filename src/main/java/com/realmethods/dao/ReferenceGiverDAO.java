/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity ReferenceGiver.
 *
 * @author Dev Team
 */

// AIB : #getDAOClassDecl()
public class ReferenceGiverDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(ReferenceGiver.class.getName());

    /**
     * default constructor
     */
    public ReferenceGiverDAO() {
    }

    /**
     * Retrieves a ReferenceGiver from the persistent store, using the provided primary key.
     * If no match is found, a null ReferenceGiver is returned.
     * <p>
     * @param       pk
     * @return      ReferenceGiver
     * @exception   ProcessingException
     */
    public ReferenceGiver findReferenceGiver(ReferenceGiverPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ReferenceGiverDAO.findReferenceGiver(...) cannot have a null primary key argument");
        }

        Query query = null;
        ReferenceGiver businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.realmethods.bo.ReferenceGiver as referencegiver where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("referencegiver.referenceGiverId = " +
                pk.getReferenceGiverId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new ReferenceGiver();
                businessObject.copy((ReferenceGiver) query.list().iterator()
                                                          .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGiverDAO.findReferenceGiver failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGiverDAO.findReferenceGiver - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all ReferenceGivers
     * @return                ArrayList<ReferenceGiver>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceGiver> findAllReferenceGiver()
        throws ProcessingException {
        ArrayList<ReferenceGiver> list = new ArrayList<ReferenceGiver>();
        ArrayList<ReferenceGiver> refList = new ArrayList<ReferenceGiver>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.realmethods.bo.ReferenceGiver");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<ReferenceGiver>) query.list();

                ReferenceGiver tmp = null;

                for (ReferenceGiver listEntry : list) {
                    tmp = new ReferenceGiver();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGiverDAO.findAllReferenceGiver failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGiverDAO.findAllReferenceGiver - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "ReferenceGiverDAO:findAllReferenceGivers() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new ReferenceGiver into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceGiver
     * @exception   ProcessingException
     */
    public ReferenceGiver createReferenceGiver(ReferenceGiver businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGiverDAO.createReferenceGiver - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGiverDAO.createReferenceGiver failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGiverDAO.createReferenceGiver - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided ReferenceGiver to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceGiver        stored entity
     * @exception   ProcessingException
     */
    public ReferenceGiver saveReferenceGiver(ReferenceGiver businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGiverDAO.saveReferenceGiver - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGiverDAO.saveReferenceGiver failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGiverDAO.saveReferenceGiver - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a ReferenceGiver from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteReferenceGiver(ReferenceGiverPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            ReferenceGiver bo = findReferenceGiver(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGiverDAO.deleteReferenceGiver - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGiverDAO.deleteReferenceGiver failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGiverDAO.deleteReferenceGiver - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
