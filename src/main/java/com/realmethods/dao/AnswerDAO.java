/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Answer.
 *
 * @author Dev Team
 */

// AIB : #getDAOClassDecl()
public class AnswerDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Answer.class.getName());

    /**
     * default constructor
     */
    public AnswerDAO() {
    }

    /**
     * Retrieves a Answer from the persistent store, using the provided primary key.
     * If no match is found, a null Answer is returned.
     * <p>
     * @param       pk
     * @return      Answer
     * @exception   ProcessingException
     */
    public Answer findAnswer(AnswerPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "AnswerDAO.findAnswer(...) cannot have a null primary key argument");
        }

        Query query = null;
        Answer businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.realmethods.bo.Answer as answer where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("answer.answerId = " +
                pk.getAnswerId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Answer();
                businessObject.copy((Answer) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "AnswerDAO.findAnswer failed for primary key " + pk + " - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AnswerDAO.findAnswer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Answers
     * @return                ArrayList<Answer>
     * @exception   ProcessingException
     */
    public ArrayList<Answer> findAllAnswer() throws ProcessingException {
        ArrayList<Answer> list = new ArrayList<Answer>();
        ArrayList<Answer> refList = new ArrayList<Answer>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.realmethods.bo.Answer");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Answer>) query.list();

                Answer tmp = null;

                for (Answer listEntry : list) {
                    tmp = new Answer();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("AnswerDAO.findAllAnswer failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AnswerDAO.findAllAnswer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("AnswerDAO:findAllAnswers() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Answer into the persistent store.
     * @param       businessObject
     * @return      newly persisted Answer
     * @exception   ProcessingException
     */
    public Answer createAnswer(Answer businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AnswerDAO.createAnswer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AnswerDAO.createAnswer failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AnswerDAO.createAnswer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Answer to the persistent store.
     *
     * @param       businessObject
     * @return      Answer        stored entity
     * @exception   ProcessingException
     */
    public Answer saveAnswer(Answer businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AnswerDAO.saveAnswer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AnswerDAO.saveAnswer failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AnswerDAO.saveAnswer - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Answer from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteAnswer(AnswerPrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Answer bo = findAnswer(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AnswerDAO.deleteAnswer - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AnswerDAO.deleteAnswer failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AnswerDAO.deleteAnswer - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
