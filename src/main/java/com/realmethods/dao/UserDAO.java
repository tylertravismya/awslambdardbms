/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity User.
 *
 * @author Dev Team
 */

// AIB : #getDAOClassDecl()
public class UserDAO extends ReferrerDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(User.class.getName());

    /**
     * default constructor
     */
    public UserDAO() {
    }

    /**
     * Retrieves a User from the persistent store, using the provided primary key.
     * If no match is found, a null User is returned.
     * <p>
     * @param       pk
     * @return      User
     * @exception   ProcessingException
     */
    public User findUser(UserPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "UserDAO.findUser(...) cannot have a null primary key argument");
        }

        Query query = null;
        User businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.realmethods.bo.User as user where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("user.referrerId = " +
                pk.getReferrerId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new User();
                businessObject.copy((User) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "UserDAO.findUser failed for primary key " + pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "UserDAO.findUser - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Users
     * @return                ArrayList<User>
     * @exception   ProcessingException
     */
    public ArrayList<User> findAllUser() throws ProcessingException {
        ArrayList<User> list = new ArrayList<User>();
        ArrayList<User> refList = new ArrayList<User>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.realmethods.bo.User");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<User>) query.list();

                User tmp = null;

                for (User listEntry : list) {
                    tmp = new User();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("UserDAO.findAllUser failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "UserDAO.findAllUser - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("UserDAO:findAllUsers() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new User into the persistent store.
     * @param       businessObject
     * @return      newly persisted User
     * @exception   ProcessingException
     */
    public User createUser(User businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "UserDAO.createUser - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("UserDAO.createUser failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "UserDAO.createUser - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided User to the persistent store.
     *
     * @param       businessObject
     * @return      User        stored entity
     * @exception   ProcessingException
     */
    public User saveUser(User businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "UserDAO.saveUser - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("UserDAO.saveUser failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "UserDAO.saveUser - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a User from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteUser(UserPrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            User bo = findUser(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "UserDAO.deleteUser - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("UserDAO.deleteUser failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "UserDAO.deleteUser - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
