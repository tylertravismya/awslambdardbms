/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;

import com.realmethods.bo.*;

import com.realmethods.dao.*;

import com.realmethods.exception.ProcessingException;

import com.realmethods.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Comment.
 *
 * @author Dev Team
 */

// AIB : #getDAOClassDecl()
public class CommentDAO extends BaseDAO // ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Comment.class.getName());

    /**
     * default constructor
     */
    public CommentDAO() {
    }

    /**
     * Retrieves a Comment from the persistent store, using the provided primary key.
     * If no match is found, a null Comment is returned.
     * <p>
     * @param       pk
     * @return      Comment
     * @exception   ProcessingException
     */
    public Comment findComment(CommentPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "CommentDAO.findComment(...) cannot have a null primary key argument");
        }

        Query query = null;
        Comment businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.realmethods.bo.Comment as comment where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("comment.commentId = " +
                pk.getCommentId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Comment();
                businessObject.copy((Comment) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "CommentDAO.findComment failed for primary key " + pk + " - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "CommentDAO.findComment - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Comments
     * @return                ArrayList<Comment>
     * @exception   ProcessingException
     */
    public ArrayList<Comment> findAllComment() throws ProcessingException {
        ArrayList<Comment> list = new ArrayList<Comment>();
        ArrayList<Comment> refList = new ArrayList<Comment>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.realmethods.bo.Comment");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Comment>) query.list();

                Comment tmp = null;

                for (Comment listEntry : list) {
                    tmp = new Comment();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("CommentDAO.findAllComment failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "CommentDAO.findAllComment - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("CommentDAO:findAllComments() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Comment into the persistent store.
     * @param       businessObject
     * @return      newly persisted Comment
     * @exception   ProcessingException
     */
    public Comment createComment(Comment businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "CommentDAO.createComment - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("CommentDAO.createComment failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "CommentDAO.createComment - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Comment to the persistent store.
     *
     * @param       businessObject
     * @return      Comment        stored entity
     * @exception   ProcessingException
     */
    public Comment saveComment(Comment businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "CommentDAO.saveComment - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("CommentDAO.saveComment failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "CommentDAO.saveComment - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Comment from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteComment(CommentPrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Comment bo = findComment(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "CommentDAO.deleteComment - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("CommentDAO.deleteComment failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "CommentDAO.deleteComment - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
