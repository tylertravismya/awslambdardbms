/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.realmethods.bo.*;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * Encapsulates data for business entity Activity.
 *
 * @author Dev Team
 */
@JsonIgnoreProperties(ignoreUnknown = true)
// AIB : #getBOClassDecl()
public class Activity extends Base {
    // attributes

    // AIB : #getAttributeDeclarations( true  )
    protected Long activityId = null;
    public Long refObjId = null;
    public java.util.Date createDateTime = null;
    protected ActivityType type = ActivityType.getDefaultValue();
    protected User user = null;

    // ~AIB

    //************************************************************************
    // Constructors
    //************************************************************************

    /**
     * Default Constructor
     */
    public Activity() {
    }

    //************************************************************************
    // Accessor Methods
    //************************************************************************

    /**
     * Returns the ActivityPrimaryKey
     * @return ActivityPrimaryKey
     */
    public ActivityPrimaryKey getActivityPrimaryKey() {
        ActivityPrimaryKey key = new ActivityPrimaryKey();
        key.setActivityId(this.activityId);

        return (key);
    }

    // AIB : #getBOAccessorMethods(true)
    /**
    * Returns the refObjId
    * @return Long
    */
    public Long getRefObjId() {
        return this.refObjId;
    }

    /**
                  * Assigns the refObjId
        * @param refObjId        Long
        */
    public void setRefObjId(Long refObjId) {
        this.refObjId = refObjId;
    }

    /**
    * Returns the createDateTime
    * @return java.util.Date
    */
    public java.util.Date getCreateDateTime() {
        return this.createDateTime;
    }

    /**
                  * Assigns the createDateTime
        * @param createDateTime        java.util.Date
        */
    public void setCreateDateTime(java.util.Date createDateTime) {
        this.createDateTime = createDateTime;
    }

    /**
    * Returns the Type
    * @return ActivityType
    */
    public ActivityType getType() {
        return this.type;
    }

    /**
                  * Assigns the type
        * @param type        ActivityType
        */
    public void setType(ActivityType type) {
        this.type = type;
    }

    /**
    * Returns the User
    * @return User
    */
    public User getUser() {
        return this.user;
    }

    /**
                  * Assigns the user
        * @param user        User
        */
    public void setUser(User user) {
        this.user = user;
    }

    /**
    * Returns the activityId
    * @return Long
    */
    public Long getActivityId() {
        return this.activityId;
    }

    /**
                  * Assigns the activityId
        * @param activityId        Long
        */
    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    // ~AIB

    /**
     * Performs a shallow copy.
     * @param object         Activity                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public Activity copyShallow(Activity object)
        throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " Activity:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        // Set member attributes

        // AIB : #getCopyString( false )
        this.activityId = object.getActivityId();
        this.refObjId = object.getRefObjId();
        this.createDateTime = object.getCreateDateTime();
        this.type = object.getType();

        // ~AIB 
        return this;
    }

    /**
     * Performs a deep copy.
     * @param object         Activity                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public Activity copy(Activity object) throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " Activity:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        copyShallow(object);

        // Set member attributes

        // AIB : #getCopyString( true )
        if (object.getUser() != null) {
            this.user = new User();
            this.user.copyShallow(object.getUser());
        } else {
            this.user = null;
        }

        // ~AIB 
        return (this);
    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString() {
        StringBuilder returnString = new StringBuilder();

        returnString.append(super.toString() + ", ");

        // AIB : #getToString( false )
        returnString.append("activityId = " + this.activityId + ", ");
        returnString.append("refObjId = " + this.refObjId + ", ");
        returnString.append("createDateTime = " + this.createDateTime + ", ");
        returnString.append("type = " + this.type + ", ");

        // ~AIB 
        return returnString.toString();
    }

    public java.util.Collection<String> getAttributesByNameUserIdentifiesBy() {
        Collection<String> names = new java.util.ArrayList<String>();

        return (names);
    }

    public String getIdentity() {
        StringBuilder identity = new StringBuilder("Activity");

        identity.append("::");
        identity.append(activityId);

        return (identity.toString());
    }

    public String getObjectType() {
        return ("Activity");
    }

    //************************************************************************
    // Object Overloads
    //************************************************************************
    public boolean equals(Object object) {
        Object tmpObject = null;

        if (this == object) {
            return true;
        }

        if (object == null) {
            return false;
        }

        if (!(object instanceof Activity)) {
            return false;
        }

        Activity bo = (Activity) object;

        return (getActivityPrimaryKey().equals(bo.getActivityPrimaryKey()));
    }

    // ~AIB
}
