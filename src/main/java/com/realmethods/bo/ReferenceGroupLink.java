/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.realmethods.bo.*;

import com.realmethods.primarykey.*;

import java.util.*;


/**
 * Encapsulates data for business entity ReferenceGroupLink.
 *
 * @author Dev Team
 */
@JsonIgnoreProperties(ignoreUnknown = true)
// AIB : #getBOClassDecl()
public class ReferenceGroupLink extends Base {
    // attributes

    // AIB : #getAttributeDeclarations( true  )
    protected Long referenceGroupLinkId = null;
    public java.util.Date dateLinkCreated = null;
    public String name = null;
    protected ReferrerGroup referrerGroup = null;
    protected User linkProvider = null;

    // ~AIB

    //************************************************************************
    // Constructors
    //************************************************************************

    /**
     * Default Constructor
     */
    public ReferenceGroupLink() {
    }

    //************************************************************************
    // Accessor Methods
    //************************************************************************

    /**
     * Returns the ReferenceGroupLinkPrimaryKey
     * @return ReferenceGroupLinkPrimaryKey
     */
    public ReferenceGroupLinkPrimaryKey getReferenceGroupLinkPrimaryKey() {
        ReferenceGroupLinkPrimaryKey key = new ReferenceGroupLinkPrimaryKey();
        key.setReferenceGroupLinkId(this.referenceGroupLinkId);

        return (key);
    }

    // AIB : #getBOAccessorMethods(true)
    /**
    * Returns the dateLinkCreated
    * @return java.util.Date
    */
    public java.util.Date getDateLinkCreated() {
        return this.dateLinkCreated;
    }

    /**
                  * Assigns the dateLinkCreated
        * @param dateLinkCreated        java.util.Date
        */
    public void setDateLinkCreated(java.util.Date dateLinkCreated) {
        this.dateLinkCreated = dateLinkCreated;
    }

    /**
    * Returns the name
    * @return String
    */
    public String getName() {
        return this.name;
    }

    /**
                  * Assigns the name
        * @param name        String
        */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * Returns the ReferrerGroup
    * @return ReferrerGroup
    */
    public ReferrerGroup getReferrerGroup() {
        return this.referrerGroup;
    }

    /**
                  * Assigns the referrerGroup
        * @param referrerGroup        ReferrerGroup
        */
    public void setReferrerGroup(ReferrerGroup referrerGroup) {
        this.referrerGroup = referrerGroup;
    }

    /**
    * Returns the LinkProvider
    * @return User
    */
    public User getLinkProvider() {
        return this.linkProvider;
    }

    /**
                  * Assigns the linkProvider
        * @param linkProvider        User
        */
    public void setLinkProvider(User linkProvider) {
        this.linkProvider = linkProvider;
    }

    /**
    * Returns the referenceGroupLinkId
    * @return Long
    */
    public Long getReferenceGroupLinkId() {
        return this.referenceGroupLinkId;
    }

    /**
                  * Assigns the referenceGroupLinkId
        * @param referenceGroupLinkId        Long
        */
    public void setReferenceGroupLinkId(Long referenceGroupLinkId) {
        this.referenceGroupLinkId = referenceGroupLinkId;
    }

    // ~AIB

    /**
     * Performs a shallow copy.
     * @param object         ReferenceGroupLink                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public ReferenceGroupLink copyShallow(ReferenceGroupLink object)
        throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " ReferenceGroupLink:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        // Set member attributes

        // AIB : #getCopyString( false )
        this.referenceGroupLinkId = object.getReferenceGroupLinkId();
        this.dateLinkCreated = object.getDateLinkCreated();
        this.name = object.getName();

        // ~AIB 
        return this;
    }

    /**
     * Performs a deep copy.
     * @param object         ReferenceGroupLink                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public ReferenceGroupLink copy(ReferenceGroupLink object)
        throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " ReferenceGroupLink:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        copyShallow(object);

        // Set member attributes

        // AIB : #getCopyString( true )
        if (object.getReferrerGroup() != null) {
            this.referrerGroup = new ReferrerGroup();
            this.referrerGroup.copyShallow(object.getReferrerGroup());
        } else {
            this.referrerGroup = null;
        }

        if (object.getLinkProvider() != null) {
            this.linkProvider = new User();
            this.linkProvider.copyShallow(object.getLinkProvider());
        } else {
            this.linkProvider = null;
        }

        // ~AIB 
        return (this);
    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString() {
        StringBuilder returnString = new StringBuilder();

        returnString.append(super.toString() + ", ");

        // AIB : #getToString( false )
        returnString.append("referenceGroupLinkId = " +
            this.referenceGroupLinkId + ", ");
        returnString.append("dateLinkCreated = " + this.dateLinkCreated + ", ");
        returnString.append("name = " + this.name + ", ");

        // ~AIB 
        return returnString.toString();
    }

    public java.util.Collection<String> getAttributesByNameUserIdentifiesBy() {
        Collection<String> names = new java.util.ArrayList<String>();

        return (names);
    }

    public String getIdentity() {
        StringBuilder identity = new StringBuilder("ReferenceGroupLink");

        identity.append("::");
        identity.append(referenceGroupLinkId);

        return (identity.toString());
    }

    public String getObjectType() {
        return ("ReferenceGroupLink");
    }

    //************************************************************************
    // Object Overloads
    //************************************************************************
    public boolean equals(Object object) {
        Object tmpObject = null;

        if (this == object) {
            return true;
        }

        if (object == null) {
            return false;
        }

        if (!(object instanceof ReferenceGroupLink)) {
            return false;
        }

        ReferenceGroupLink bo = (ReferenceGroupLink) object;

        return (getReferenceGroupLinkPrimaryKey()
                    .equals(bo.getReferenceGroupLinkPrimaryKey()));
    }

    // ~AIB
}
